#ifndef __LCD_H_
#define __LCD_H_

#include "touchscreen.h"
#define LCD_DC		PG4
#define LCD_CS		PG0
#define LCD_RD		PG2
#define LCD_WR		PG1
#define LCD_RESET	PG3

#define LCD_CTRL_PORT	PORTG
#define LCD_CTRL_DDR	DDRG


#define LCD_DATA_LOW 	PORTD
#define LCD_DATA_MED	PORTC
#define LCD_DATA_HIGH	PORTA

#define LCD_DATA_LOW_DDR	DDRD
#define LCD_DATA_MED_DDR	DDRC
#define LCD_DATA_HIGH_DDR	DDRA

#define CHARGE_PUMP_DDR		DDRE
#define CHARGE_PUMP_PORT	PORTE
#define CHARGE_PUMP_PIN		PE3




typedef struct Color
	{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	} COLOR;

typedef struct Lcd_rect	
{
	unsigned char left; 	/*!< The left side of the rectangle */
	unsigned char top; 		/*!< The top position of the rectangle */
	unsigned char right; 	/*!< The right side of the rectangle */
	unsigned char bottom; 	/*!< The bottom position of the rectangle */
}LCD_RECT;


typedef struct Bmp
	{
	unsigned char 	width;
	unsigned char 	height;
	unsigned int 	length;
	COLOR			pixels[];
	} BMP;

#ifdef __cplusplus
extern "C"{
#endif

void lcd_init();
void lcd_setRow(unsigned char start, unsigned char end);
void lcd_setColumn(unsigned char start, unsigned char end);
void lcd_write_C(unsigned char command);
void lcd_write_D(unsigned char data);
void lcd_pix();
unsigned char lcd_read_status(void);

void lcd_clear(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2, COLOR * color);
void lcd_clearScreen(COLOR c);
void lcd_setBrightness(unsigned char brightnessLevel);
void lcd_setContrast(unsigned char red, unsigned char green, unsigned char blue);

void lcd_pixel(unsigned char x, unsigned char y, COLOR pixel_color);
void lcd_circle(unsigned char x, unsigned char y, unsigned char radius, COLOR outline_color, COLOR fill_color);
void lcd_rectangle(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, COLOR outline_color, COLOR fill_color);
void lcd_line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, COLOR line_color);

void lcd_putc(unsigned char ch, unsigned char x_pos,unsigned char y_pos,COLOR fc, COLOR bc);
void lcd_puts(char * string,unsigned char x_pos, unsigned char y_pos, COLOR fc, COLOR bc);
void lcd_dimWindow(unsigned char left, unsigned char top, unsigned char right, unsigned char bottom);  

void lcd_rect(LCD_RECT r, COLOR outline_color, COLOR fill_color);

char pointInRect(POINT p, LCD_RECT r);

#ifdef __cplusplus
} // extern "C"
#endif



#endif

